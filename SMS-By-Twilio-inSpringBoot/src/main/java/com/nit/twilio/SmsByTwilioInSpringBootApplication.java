package com.nit.twilio;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

@SpringBootApplication
public class SmsByTwilioInSpringBootApplication implements CommandLineRunner {

	private final static String ACCOUNT_SID = "ACe86efafb8b63f732184843e99b0f7d6a";
	private final static String AUTH_TOKEN = "dc77d2c642d8604e48cfbc3e40559ad7";

	public static void main(String[] args) {
		SpringApplication.run(SmsByTwilioInSpringBootApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
		// Message.creator(new PhoneNumber("+918570919372"),new
		// PhoneNumber("+16822444272"), "Hi welcome to twilio message").create();
		Message.creator(new PhoneNumber("+918249375599"), new PhoneNumber("+16822444272"),
				"Hi welcome to twilio message").create();
	}
}
