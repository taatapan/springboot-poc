package com.nit.certificate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootSSlCertificateApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootSSlCertificateApplication.class, args);
	}

}

