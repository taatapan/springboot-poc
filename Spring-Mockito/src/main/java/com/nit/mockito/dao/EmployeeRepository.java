package com.nit.mockito.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nit.mockito.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Integer>{

}
