package com.nit.tomcat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class SpringBootExternalTomcatApplication {

	@GetMapping("/test")
	public String test() {
		return "Application test";
	}
	public static void main(String[] args) {
		SpringApplication.run(SpringBootExternalTomcatApplication.class, args);
	}
}
