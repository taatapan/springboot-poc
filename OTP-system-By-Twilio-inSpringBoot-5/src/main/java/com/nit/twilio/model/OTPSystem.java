package com.nit.twilio.model;
public class OTPSystem {

	private String mobilenumber;
	private String otp;
	private long expirytime;
	public OTPSystem() {
		super();
	}
	public OTPSystem(String mobilenumber, String otp, long expirytime) {
		super();
		this.mobilenumber = mobilenumber;
		this.otp = otp;
		this.expirytime = expirytime;
	}
	public String getMobilenumber() {
		return mobilenumber;
	}
	public void setMobilenumber(String mobilenumber) {
		this.mobilenumber = mobilenumber;
	}
	public String getOtp() {
		return otp;
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}
	public long getExpirytime() {
		return expirytime;
	}
	public void setExpirytime(long expirytime) {
		this.expirytime = expirytime;
	}
	@Override
	public String toString() {
		return "OTPSystem [mobilenumber=" + mobilenumber + ", otp=" + otp + ", expirytime=" + expirytime + "]";
	}
	
	
	
}
