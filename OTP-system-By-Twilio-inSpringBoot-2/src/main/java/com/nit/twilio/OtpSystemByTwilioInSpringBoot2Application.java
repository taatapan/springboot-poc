package com.nit.twilio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OtpSystemByTwilioInSpringBoot2Application {

	public static void main(String[] args) {
		SpringApplication.run(OtpSystemByTwilioInSpringBoot2Application.class, args);
	}
}
