package com.nit.swagger.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "Swagger2 WelcomeRest Controller", description = "This REST Api related to Welcome Message!!!!")
@RestController
@RequestMapping("/rest")
public class SwaggerController {
	
	@ApiOperation(value = "Get Welcome Message For The Given Name ", response = String.class, tags = "Get Welcome Note")
	@GetMapping("/greet")
	public String greet() {
		return "welcome to swagger";
	}

}
