package com.nit.swagger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestFulApiUsingSwaggerApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestFulApiUsingSwaggerApplication.class, args);
	}
}
