package com.nit.ldap.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest")
public class ApplicationController {
	
	@GetMapping("/show")
	public String secureMethod() {
		return "secure rest endpoint";
	}

}
