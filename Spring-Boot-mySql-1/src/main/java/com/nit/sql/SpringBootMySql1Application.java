package com.nit.sql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootMySql1Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootMySql1Application.class, args);
	}
}
