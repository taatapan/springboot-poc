package com.nit.schedular.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nit.schedular.model.User;

public interface UserDao extends JpaRepository<User, String>{

}
