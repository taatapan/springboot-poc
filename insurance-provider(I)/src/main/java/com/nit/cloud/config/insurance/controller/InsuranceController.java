package com.nit.cloud.config.insurance.controller;


import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/insurance-service")
public class InsuranceController {

	@GetMapping("/planupdates")
	public List<String> getplans(){
		return Stream.of("Premium","Gold","Silver","Bronze").collect(Collectors.toList());
	}
}
