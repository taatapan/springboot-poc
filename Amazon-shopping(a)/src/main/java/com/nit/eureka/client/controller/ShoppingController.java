package com.nit.eureka.client.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/shopping")
public class ShoppingController {

	@Autowired
	private RestTemplate template;

	@GetMapping("/amazon-payment/{price}")
	public String invokePaymentService(@PathVariable int price) {

		// Traditional approch to access payment service
		// String url="http://localhost:9090/payment/pay/"+price;

		String url = "http://PAYMENT-SERVICE/payment/pay/" + price;
		return template.getForObject(url, String.class);

	}

}
