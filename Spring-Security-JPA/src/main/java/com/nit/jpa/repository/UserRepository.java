package com.nit.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nit.jpa.model.User;

public interface UserRepository  extends JpaRepository<User, Integer>{

	User findByUsername(String username);

}
