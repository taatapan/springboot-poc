package com.nit.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nit.jpa.model.Role;


public interface RoleRepository extends JpaRepository<Role, Integer>{

}
