package com.nit.template.controller;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.nit.template.model.User;

@Controller
public class ApplicationController {
	
@GetMapping("/greeting/{input}")
public String greeting(@PathVariable String input,Model model) {
	String welcomeMessage="Hi "+input+" welcome to freemarker";
model.addAttribute("message",welcomeMessage);
 return "welcome";
}
@GetMapping("/getUsers")
public String getUsers(Model model) {
List<User> users= 	Stream.of(new User(1,"tapan", "India"),new User(2,"kumar","Australia"),new User(3,"biswas","london")).collect(Collectors.toList());
model.addAttribute("users",users);	
return "userList";
}

}
