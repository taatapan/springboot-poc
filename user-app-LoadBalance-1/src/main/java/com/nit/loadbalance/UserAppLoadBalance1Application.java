package com.nit.loadbalance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.context.annotation.Bean;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.nit.loadbalance.config.RibbonConfiguration;

@SpringBootApplication
@RestController
@RibbonClient(name= "ChatBook", configuration = RibbonConfiguration.class)
public class UserAppLoadBalance1Application {

	@Autowired
	private RestTemplate template;

	@GetMapping("/invoke")
	public String invokeChatbook() {
		return template.getForObject("http://ChatBook/chatbook-application/chat" , String.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(UserAppLoadBalance1Application.class, args);
	}

	@Bean
	@LoadBalanced
	public RestTemplate template() {
		return new RestTemplate();
	}
}
