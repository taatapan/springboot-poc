package com.nit.sql;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.nit.sql.dao.EmployeeDao;
import com.nit.sql.model.Employee;

@SpringBootApplication
public class SpringBootMySqlApplication implements CommandLineRunner {
	@Autowired
	private EmployeeDao employeeDao;

	public static void main(String[] args) {
		SpringApplication.run(SpringBootMySqlApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		Employee employee = getEmployee();
		employeeDao.createEmployee(employee);
	}

	private Employee getEmployee() {
		Employee employee = new Employee();
		employee.setName("Tapan Kumar");
		employee.setSalary(80000.00);
		employee.setDoj(new Date());
		return employee;
	}
}
