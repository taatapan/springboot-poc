package com.nit.logging.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class LoggingController {
	
	Logger log=LoggerFactory.getLogger(LoggingController.class);
	
	
	@GetMapping("/test/{name}")
	public String greet(@PathVariable String name) {
		log.debug("Request{}",name);
		if(name.equalsIgnoreCase("test")) {
			throw new RuntimeException("oops Exception raised...");
		}
		String response="HI " + name + " Welcome to Logging usin sl4j";
		log.debug("Response {}", response);
		return response;
	}

}
