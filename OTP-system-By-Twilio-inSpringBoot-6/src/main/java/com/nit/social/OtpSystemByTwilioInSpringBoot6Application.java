package com.nit.social;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OtpSystemByTwilioInSpringBoot6Application {

	public static void main(String[] args) {
		SpringApplication.run(OtpSystemByTwilioInSpringBoot6Application.class, args);
	}
}
