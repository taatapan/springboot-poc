package com.nit.twilio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@SpringBootApplication
public class ProgrammablevoicePhonecallByTwilioInSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProgrammablevoicePhonecallByTwilioInSpringBootApplication.class, args);
	
	/*@RequestMapping(value="/voice-note", method=RequestMethod.POST, produces=MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<Object> getVoiceNote(){
		
	}*/
	}
}
