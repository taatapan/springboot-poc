package com.example.tapan.security.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/noauth")
public class NoAuthController {

	@GetMapping("/tapan")
	public String sayHi() {
		return "hi";
	}
}
